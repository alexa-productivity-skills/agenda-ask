# AGENDA #



### What is AGENDA? ###

**AGENDA (Aggregated Group of Entries for Networking, Dates, and Appointments)** is an **Alexa** skill for productivity and collaboration 
that uses **APL**, **APL for Audio**, **motion and sensing APIs**, and **Alexa Web API for Games**.